import React, { Component } from "react";
import styled from "styled-components";

const PresentProjectContainer = styled.div``;
const ProjectSection = styled.div``;

class PresentProject extends Component {
  render() {
    return (
      <PresentProjectContainer>
        <ProjectSection>Hello</ProjectSection>
      </PresentProjectContainer>
    );
  }
}

export default PresentProject;
